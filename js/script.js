document.addEventListener("DOMContentLoaded", function () {
    class DOMElementsProvider {
        getButtonClickSound() {
            return document.querySelector(".button-click-sound");
        }
        getTurboModeSwitcherCheckbox() {
            return document.querySelector(".turbo-switcher__checkbox");
        }
        getInfoButton() {
            return document.querySelector(".info-button");
        }
        getMinusButton() {
            return document.querySelector(".bet-field__button_minus");
        }
        getPlusButton() {
            return document.querySelector(".bet-field__button_plus");
        }
        getSpinIcon() {
            return document.querySelector(".spin-button__icon-spin");
        }
        getSpinDisabledIcon() {
            return document.querySelector(".spin-button__icon-spin_disabled");
        }
        getStopIcon() {
            return document.querySelector(".stop-button__icon-stop");
        }
        getStopDisabledIcon() {
            return document.querySelector(".stop-button__icon-stop_disabled");
        }
        getSpinButton() {
            return document.querySelector(".spin-button");
        };
        getStopButton() {
            return document.querySelector(".stop-button");
        };
        getWindow() {
            return document.querySelector(".window__text");
        }
        getLimitedSpinsAmountLabel() {
            return document.querySelector(".auto-spin__spins-left-value");
        }
        get5SpinsRunner() {
            return document.querySelector(".auto-spin__options-list-item_5");
        };
        get10SpinsRunner() {
            return document.querySelector(".auto-spin__options-list-item_10");
        };
        get15SpinsRunner() {
            return document.querySelector(".auto-spin__options-list-item_15");
        };
        get20SpinsRunner() {
            return document.querySelector(".auto-spin__options-list-item_20");
        };
        get25SpinsRunner() {
            return document.querySelector(".auto-spin__options-list-item_25");
        };
        getUntilFeatureSpinsRunner() {
            return document.querySelector(".auto-spin__options-list-item_until-feature");
        };
        getStopLimitedSpinsButton() {
            return document.querySelector(".auto-spin__limited-spins-mode")
        };
        getStopUnlimitedSpinsButton() {
            return document.querySelector(".auto-spin__unlimited-spins-mode")
        }
        getStopUnlimitedSpinsIcon() {
            return document.querySelector(".auto-spin__unlimited-spins-sign")
        }
        getDefaultAutoSpinLabel() {
            return document.querySelector(".auto-spin__default-mode")
        }
        getLimitedSpinsStopButton() {
            return document.querySelector(".auto-spin__limited-spins-mode")
        };
        getUnlimitedSpinsStopButton() {
            return document.querySelector(".auto-spin__unlimited-spins-mode")
        }
        getMinusButton() {
            return document.querySelector(".bet-field__button_minus");
        }
        getPlusButton() {
            return document.querySelector(".bet-field__button_plus");
        }
        getTotalBetField() {
            return document.querySelector(".bet-field__info-label-value");
        }
        getCoinField() {
            return document.querySelector(".coin-field__info-label-value");
        }
        getTurboModeSwitcherBackground() {
            return document.querySelector(".turbo-switcher");
        }
        getTurboModeSwitcher() {
            return document.querySelector(".turbo-switcher__label");
        }
        getTurboModeSwitcherCheckbox() {
            return document.querySelector(".turbo-switcher__checkbox");
        }
    }
    class DisplayManager {
        setValue(value) {
            DOM.getWindow().innerHTML = value;
        }
    }
    const display = new DisplayManager();

    class Spinner {
        _isFeature() {
            const featRand = Math.random();
            const isFeature = Boolean(featRand > 0.9);
            return isFeature;
        }
        _getWinSum() {
            const betVariants = betField.getBetVariants();
            const currentBetIndex = betField.getCurrentBetIndex();

            const betSum = betVariants[currentBetIndex].total;
            const rangeOfPossibleWin = [0, betSum*1, 0, betSum*2, 0, betSum*10, 0, betSum*3, 0, 0, 0, 0, betSum*5, 0, betSum*4, 0];
            const randOfRange = Math.floor(Math.random()*(rangeOfPossibleWin.length-1));
            const win = rangeOfPossibleWin[randOfRange];
            return win;
        }
        _getPrizeObject() {
            const isFeature = this._isFeature();
            const win = this._getWinSum();

            if (win === 0) {
                display.setValue("GOOD LUCK!");
            } else {
                display.setValue(`$${win}`);
            }
            return {
                isFeature,
                win
            };
        };
        handleMultipleSpinsDisruption(timeout) {
            DOM.getStopLimitedSpinsButton().addEventListener("click", () => {
                clearTimeout(timeout);
            });
            DOM.getStopUnlimitedSpinsButton().addEventListener("click", () => {
                clearTimeout(timeout);
            });
        }
        async runSpin() {
            setStopButtonDisabled();
            return new Promise((resolve, reject) => {
                const timeout = setTimeout(() => {
                    resolve(setStopButtonActive())
                }, 500);
                this.handleMultipleSpinsDisruption(timeout);
            })
            .then(() => {
                return new Promise((resolve, reject) => {
                    const timeout = setTimeout(() => {
                        resolve(setSpinButtonActive());
                    }, 1000);
                    this.handleMultipleSpinsDisruption(timeout);
                })
            })
            .then(() => {
                return this._getPrizeObject();
            })
        };
        async stopSpin() {
            setSpinButtonDisabled();
            setTimeout(setSpinButtonActive, 500);
        }
    }
    

    function discardAllIcons() {
        DOM.getSpinIcon().style.display = "none";
        DOM.getSpinDisabledIcon().style.display = "none";
        DOM.getStopIcon().style.display = "none";
        DOM.getStopDisabledIcon().style.display = "none";
    }

    function discardAllButtons() {
        DOM.getStopButton().style.display = "none";
        DOM.getSpinButton().style.display = "none";
    }

    function setSpinButtonDisabled() {
        discardAllButtons();
        DOM.getSpinButton().style.display = "flex";
        DOM.getSpinButton().disabled = true;
        discardAllIcons();
        DOM.getSpinDisabledIcon().style.display = "block";
    }
    function setSpinButtonActive() {
        discardAllButtons();
        DOM.getSpinButton().style.display = "flex";
        DOM.getSpinButton().disabled = false;
        discardAllIcons();
        DOM.getSpinIcon().style.display = "block";
    }
    function setStopButtonDisabled() {
        discardAllButtons();
        DOM.getStopButton().style.display = "flex";
        DOM.getStopButton().disabled = true;
        discardAllIcons();
        DOM.getStopDisabledIcon().style.display = "block";
    }
    function setStopButtonActive() {
        discardAllButtons();
        DOM.getStopButton().style.display = "flex";
        DOM.getStopButton().disabled = false;
        discardAllIcons();
        DOM.getStopIcon().style.display = "block";
    }

    class MultipleSpinner {
        runSpins() {}
        handleStopSpins() {
            spinner.handleMultipleSpinsDisruption();
        }
    };

    class LimitedMultipleSpinner extends MultipleSpinner {
        _renderAmountOfSpins(amount) {
            DOM.getLimitedSpinsAmountLabel().innerHTML = amount;
        }
        async runSpins(amount) {
            for (let i = amount-1; i >= 0; i--) {
                this._renderAmountOfSpins(i);
                await spinner.runSpin();
            }
        }
        handleStopSpins() {
            spinner.handleMultipleSpinsDisruption();
        }
    }

    class UntilFeatureMultipleSpinner extends MultipleSpinner {
        _addAnimationToSpinIcon() {
            DOM.getStopUnlimitedSpinsIcon().style.animation = "spin-icon-animation 1.5s linear infinite";
            setTimeout(() => {
                DOM.getStopUnlimitedSpinsIcon().style.animation = "none";
            }, 1500);
        }
        async runSpins() {
            while (true) {
                this._addAnimationToSpinIcon();
                const spin = await spinner.runSpin();
                if (spin.isFeature) {
                    break;
                }
            }
        }
        handleStopSpins() {
            spinner.handleMultipleSpinsDisruption();
        }
    }
    class ControlsLocker {
        lock() {
            DOM.getPlusButton().disabled = true;
            DOM.getMinusButton().disabled = true;
            DOM.getInfoButton().disabled = true;
            DOM.getTurboModeSwitcherCheckbox().disabled = true;
        };
        unlock() {
            DOM.getPlusButton().disabled = false;
            DOM.getMinusButton().disabled = false;
            DOM.getInfoButton().disabled = false;
            DOM.getTurboModeSwitcherCheckbox().disabled = false;
        }
    }
    class AutoSpinnerModeSwitcher {
        switchToLimitedSpinsMode() {
            controlsLocker.lock();
            DOM.getDefaultAutoSpinLabel().style.display = "none";
            DOM.getUnlimitedSpinsStopButton().style.display = "none";
            DOM.getLimitedSpinsStopButton().style.display = "flex";
        };
        switchToDefaultAutoSpinMode() {
            controlsLocker.unlock();
            DOM.getUnlimitedSpinsStopButton().style.display = "none";
            DOM.getLimitedSpinsStopButton().style.display = "none";
            DOM.getDefaultAutoSpinLabel().style.display = "flex";
        }
        switchToUnlimitedSpinsMode() {
            controlsLocker.lock();
            DOM.getDefaultAutoSpinLabel().style.display = "none";
            DOM.getLimitedSpinsStopButton().style.display = "none";
            DOM.getUnlimitedSpinsStopButton().style.display = "flex";
        };
    }

    class BetField {
        constructor(currentBetIndex, betVariants) {
            this.currentBetIndex = currentBetIndex;
            this.betVariants = betVariants;
        }
        renderTotalBetValue() {
            if (this.currentBetIndex >= 16) {
                DOM.getPlusButton().disabled = true;
            }
            else if (this.currentBetIndex <= 0) {
                DOM.getMinusButton().disabled = true;
            }
            else {
                DOM.getPlusButton().disabled = false;
                DOM.getMinusButton().disabled = false;
            }
            DOM.getTotalBetField().innerHTML = "$" + this.betVariants[this.currentBetIndex].total;
            DOM.getCoinField().innerHTML = "$" + this.betVariants[this.currentBetIndex].coin;
        }
        getBetVariants() {
            return this.betVariants;
        }
        getCurrentBetIndex() {
            return this.currentBetIndex;
        }
        setCurrentBetIndex(index) {
            this.currentBetIndex = index;
        }
    }
    

    class TurboModeSwitcher {
        toggle() {
            DOM.getTurboModeSwitcherBackground().classList.toggle("turbo-switcher_on");
            DOM.getTurboModeSwitcher().classList.toggle("turbo-switcher__label_on");
        }
    }
    
    class Controller {
        _addSpinButtonHandler() {
            DOM.getSpinButton().addEventListener("click", async () => {
                DOM.getButtonClickSound().play();
                controlsLocker.lock();
                await spinner.runSpin();
                controlsLocker.unlock();
            });
        };
        _addStopButtonHandler() {
            DOM.getStopButton().addEventListener("click", () => {
                DOM.getButtonClickSound().play();
                spinner.stopSpin();
                controlsLocker.unlock();
            });
        };
        _addStopLimitedSpinsButtonHandler() {
            DOM.getStopLimitedSpinsButton().addEventListener("click", () => {
                DOM.getButtonClickSound().play();
                autoSpinnerModeSwitcher.switchToDefaultAutoSpinMode();
                spinner.stopSpin();
            });
        };
        _addStopUnlimitedSpinsButtonHandler() {
            DOM.getStopUnlimitedSpinsButton().addEventListener("click", () => {
                DOM.getButtonClickSound().play();
                autoSpinnerModeSwitcher.switchToDefaultAutoSpinMode();
                spinner.stopSpin();
            });
        };
        _addSpin5TimesButtonHandler() {
            DOM.get5SpinsRunner().addEventListener("click", async () => {
                DOM.getButtonClickSound().play();
                autoSpinnerModeSwitcher.switchToLimitedSpinsMode();
                await limitedMultiSpinner.runSpins(5);
                autoSpinnerModeSwitcher.switchToDefaultAutoSpinMode();
            });
        };
        _addSpin10TimesButtonHandler() {
            DOM.get10SpinsRunner().addEventListener("click", async () => {
                DOM.getButtonClickSound().play();
                autoSpinnerModeSwitcher.switchToLimitedSpinsMode();
                await limitedMultiSpinner.runSpins(10);
                autoSpinnerModeSwitcher.switchToDefaultAutoSpinMode();
            });
        };
        _addSpin15TimesButtonHandler() {
            DOM.get15SpinsRunner().addEventListener("click", async () => {
                DOM.getButtonClickSound().play();
                autoSpinnerModeSwitcher.switchToLimitedSpinsMode();
                await limitedMultiSpinner.runSpins(15);
                autoSpinnerModeSwitcher.switchToDefaultAutoSpinMode();
            });
        };
        _addSpin20TimesButtonHandler() {
            DOM.get20SpinsRunner().addEventListener("click", async () => {
                DOM.getButtonClickSound().play();
                autoSpinnerModeSwitcher.switchToLimitedSpinsMode();
                await limitedMultiSpinner.runSpins(20);
                autoSpinnerModeSwitcher.switchToDefaultAutoSpinMode();
            });
        };
        _addSpin25TimesButtonHandler() {
            DOM.get25SpinsRunner().addEventListener("click", async () => {
                DOM.getButtonClickSound().play();
                autoSpinnerModeSwitcher.switchToLimitedSpinsMode();
                await limitedMultiSpinner.runSpins(25);
                autoSpinnerModeSwitcher.switchToDefaultAutoSpinMode();
            });
        };
        _addSpinUntilFeatureButtonHandler() {
            DOM.getUntilFeatureSpinsRunner().addEventListener("click", async () => {
                DOM.getButtonClickSound().play();
                autoSpinnerModeSwitcher.switchToUnlimitedSpinsMode();
                await untilFeaturedMultiSpinner.runSpins();
                autoSpinnerModeSwitcher.switchToDefaultAutoSpinMode();
            });
        };
        _addHandlerToPlusButtonOfBetField() {
            DOM.getPlusButton().addEventListener("click", function (event) {
                event.preventDefault();
        
                DOM.getButtonClickSound().play();
                let currentBetIndex = betField.getCurrentBetIndex();
                if (currentBetIndex < 16) {
                    betField.setCurrentBetIndex(++currentBetIndex);
                    betField.renderTotalBetValue();
                }
            });
        };
        _addHandlerToMinusButtonOfBetField() {
            DOM.getMinusButton().addEventListener("click", (event) => {
                event.preventDefault();
        
                DOM.getButtonClickSound().play();
                let currentBetIndex = betField.getCurrentBetIndex();
                if (currentBetIndex > 0) {
                    betField.setCurrentBetIndex(--currentBetIndex);
                    betField.renderTotalBetValue();
                }
            });
        };
        _addTurboModeSwitcherHandler() {
            DOM.getTurboModeSwitcherCheckbox().addEventListener("change", () => {
                DOM.getButtonClickSound().play();
                turboModeSwitcher.toggle();
            });
        };
        init() {
            this._addSpinButtonHandler();
            this._addStopButtonHandler();

            this._addStopLimitedSpinsButtonHandler();
            this._addStopUnlimitedSpinsButtonHandler();

            this._addSpin5TimesButtonHandler();
            this._addSpin10TimesButtonHandler();
            this._addSpin15TimesButtonHandler();
            this._addSpin20TimesButtonHandler();
            this._addSpin25TimesButtonHandler();
            this._addSpinUntilFeatureButtonHandler();

            this._addHandlerToPlusButtonOfBetField();
            this._addHandlerToMinusButtonOfBetField();

            this._addTurboModeSwitcherHandler();

            betField.renderTotalBetValue();
        }
    };

    const DOM = new DOMElementsProvider();
    const controlsLocker = new ControlsLocker();
    const spinner = new Spinner();
    const limitedMultiSpinner = new LimitedMultipleSpinner();
    const untilFeaturedMultiSpinner = new UntilFeatureMultipleSpinner();
    const autoSpinnerModeSwitcher = new AutoSpinnerModeSwitcher();
    const turboModeSwitcher = new TurboModeSwitcher();

    const betVariants = [
        { total: 0.15, coin: 0.01 },
        { total: 0.30, coin: 0.02 },
        { total: 0.45, coin: 0.03 },
        { total: 0.75, coin: 0.05 },
        { total: 1.20, coin: 0.08 },
        { total: 1.50, coin: 0.1 },
        { total: 3, coin: 0.2 },
        { total: 4.50, coin: 0.3 },
        { total: 7.50, coin: 0.5 },
        { total: 12, coin: 0.8 },
        { total: 15, coin: 1 },
        { total: 30, coin: 2 },
        { total: 45, coin: 3 },
        { total: 75, coin: 5 },
        { total: 120, coin: 8 },
        { total: 150, coin: 10 },
        { total: 300, coin: 20 }
    ];
    const currentBetIndex = 5;
    const betField = new BetField(currentBetIndex, betVariants);

    new Controller().init();
});